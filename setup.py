from setuptools import setup, find_packages

setup(
    name="hello_world",
    packages=['hello_world'],
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
)
