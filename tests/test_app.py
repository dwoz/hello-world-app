import pytest

from hello_world.app import app


@pytest.fixture
def client():
    app.config['TESTING'] = True
    yield app.test_client()


def test_get_without_accept(client):
    '''
    A GET request with no accept header returns HTML
    '''
    resp = client.get('/')
    assert resp.status_code == 200
    assert resp.data == b'<p>Hello, World!</p>'


def test_get_with_accept(client):
    '''
    A GET request with JSON accept header returns JSON
    '''
    resp = client.get('/', headers={'Accept': 'application/json'})
    assert resp.status_code == 200
    assert resp.json == {'message': 'good morning'}

def test_get_with_accept_name(client):
    '''
    A GET request with JSON accept header returns JSON
    '''
    resp = client.get('/world', headers={'Accept': 'application/json'})
    assert resp.status_code == 200
    assert resp.json == {'msgs': ['Hello world!']}

def test_get_with_accept_name_spanish(client):
    '''
    A GET request with JSON accept header returns JSON
    '''
    resp = client.get('/world?lang=es', headers={'Accept': 'application/json'})
    assert resp.status_code == 200
    assert resp.json == {'msgs': ['Hola world!']}

def test_get_with_accept_name_english(client):
    '''
    A GET request with JSON accept header returns JSON
    '''
    resp = client.get('/world?lang=en', headers={'Accept': 'application/json'})
    assert resp.status_code == 200
    assert resp.json == {'msgs': ['Hello world!']}

def test_post_without_accept(client):
    '''
    A POST request with no accept header returns HTML
    '''
    resp = client.post('/')
    assert resp.status_code == 200
    assert resp.data == b'<p>Hello, World!</p>'

def test_post_without_accept_name(client):
    '''
    A POST request with no accept header returns HTML
    '''
    resp = client.post('/world')
    assert resp.status_code == 200
    assert resp.data == b'Hello world!'

def test_post_without_accept_name_b(client):
    '''
    A POST request with no accept header returns HTML
    '''
    resp = client.post('/john')
    assert resp.status_code == 200
    assert resp.data == b'Hello john!'

def test_post_without_accept_name_spanish(client):
    '''
    A POST request with no accept header returns HTML
    '''
    resp = client.post('/john?lang=es')
    assert resp.status_code == 200
    assert resp.data == b'Hola john!'

def test_post_with_accept(client):
    '''
    A POST request with JSON accept header returns HTML, not JSON
    '''
    resp = client.post('/', headers={'accept': 'application/json'})
    assert resp.status_code == 200
    assert resp.data == b'<p>Hello, World!</p>'


def test_put_with_accept(client):
    '''
    PUT requests return 405
    '''
    resp = client.put('/', headers={'Accept': 'application/json'})
    assert resp.status_code == 405
    resp = client.put('/')
    assert resp.status_code == 405
