'''
Hello world application
'''
import logging
import json

from flask import Flask, request, url_for, jsonify

log = logging.getLogger()
app = Flask(__name__)


def do_json(request):
    '''
    True when the request should return a JSON response.

    JSON is only returned for GET requests with an 'Accept' header of
    'application/json'
    '''
    if request.method == 'GET':
        if request.headers.get('accept', '') == 'application/json':
            return True
    return False


def message_template(lang):
    return {
        'en': 'Hello {}!',
        'es': 'Hola {}!',
    }[lang.lower()]

@app.before_request
def before_request():
    '''
    Emit debug log for request
    '''
    log.debug("Incomming request: %s %s", request.method, request.path)
    pass


@app.route('/', methods=['GET', 'POST'])
def hello_world():
    '''
    Hello World root endpoint
    '''
    if do_json(request):
        return jsonify({'message': 'good morning'})
    return '<p>Hello, World!</p>'

@app.route('/<username>', methods=['GET', 'POST'])
def hello_name(username):
    '''
    Hello World root endpoint
    '''
    lang = request.args.get('lang', 'en')
    try:
        template = message_template(lang)
    except KeyError:
        log.error("Bad language %s", lang)
        return 'Bad request', 400
    if do_json(request):
        return jsonify({'msgs': [template.format(username)]})
    return template.format(username)
