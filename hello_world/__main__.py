import logging
from .app import app

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(message)s')
    logging.getLogger('werkzeug').setLevel(logging.ERROR)
    app.run(debug=False)
