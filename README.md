Hello World Application
=======================


This application is a simple hello world. It consists of a single enpoint at
the root of the application. The endpoint will return a JSON response for `GET`
requests with an `Accept` header of `application/json`. All other `GET`
requests will return an HTML response. All `POST` requests will return an HTML
response.



